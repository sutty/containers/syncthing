#!/bin/sh

pid=/tmp/syncthing.pid
dir=/home/syncthing
log=${dir}/syncthing.log

rm -f ${pid}

install -dm 2750 -o syncthing -g syncthing ${dir}/Sync/.stfolder

daemonize -p ${pid} -l ${pid} -u syncthing -c ${dir} \
  -o ${log} -a -e ${log} \
  /usr/bin/syncthing -no-browser -gui-address='http://0.0.0.0:8443'
