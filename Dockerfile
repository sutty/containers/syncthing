FROM alpine:latest AS build
MAINTAINER "f <f@sutty.nl>"

COPY ./syncthing_start.sh /usr/local/bin/syncthing_start
RUN chmod 755 /usr/local/bin/syncthing_start

FROM sutty/monit:latest

# Syncthing runs with the same uid and gid for Sutty so ownership is
# kept, since ST doesn't sync these.
RUN addgroup -g 82 -S syncthing
RUN adduser -s /bin/sh -G syncthing -D -u 1000 -h /home/syncthing syncthing

RUN apk add --no-cache ca-certificates daemonize syncthing

COPY ./monit.conf /etc/monit.d/syncthing.conf
COPY --from=build /usr/local/bin/syncthing_start /usr/local/bin/syncthing_start

EXPOSE 22000
EXPOSE 22000/udp

VOLUME /home/syncthing
